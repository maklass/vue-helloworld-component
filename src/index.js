import Vue from 'vue';
import HelloWorld from './HelloWorld.vue';

const helloWorldComponent = {
  install(Vue, options) {
    // if (!store) {
    //   throw new Error("Please provide vuex store.");
    // }

    // store.registerModule({
    //   states,
    //   mutations,
    //   actions
    // });

    Vue.component('vue-helloworld', HelloWorld);
  }
};

export default helloWorldComponent;
